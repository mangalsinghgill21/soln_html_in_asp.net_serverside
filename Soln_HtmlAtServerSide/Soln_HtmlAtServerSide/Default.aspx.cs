﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Soln_HtmlAtServerSide
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected async  void btnsubmit_Click(object sender, EventArgs e)
        {
            await this.AddCssProperty();

            //span1id.Style["color"] = "green";
        }

        #region Private Function
        private async Task AddCssProperty()
        {
            await Task.Run(() =>
            {
                span1id.Style["color"] = "green";
               
            });
        }
        #endregion

    }
}